<?php
/*
Autor   : Luca.BASSI
Date    : 28.02.2020
*/


function goPanier()
{
    $_GET['action'] = "goPanier";
    require 'view/panier.php';
}


function getSnows()
{
    require_once "model/modele_snows.php";
    try {
        $allSnows = extractSnows();
        require 'view/snows.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function snow()
{
    $id = $_POST["id"];
    require_once "model/modele_snows.php";
    try {
        $aSnow = extractASnow($id);
        require 'view/snow.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function addPanier()
{
    $id = $_POST["id"];
    $qty = $_POST["qtySelect"];


    require_once "model/modele_snows.php";
    try {
        $aSnow = extractASnow($id);

        $myIndex = 0;
        if (isset($_SESSION['snow'])) {
            foreach ($_SESSION['snow'] as $wesh) {
                $myIndex++;
            }
        }



        $_SESSION['snow'][$myIndex]['qty'] = $qty;
        $_SESSION['snow'][$myIndex]['totalQty'] = $aSnow[0]["qtyAvailable"];

        $_SESSION['snow'][$myIndex]['id'] = $aSnow[0]["id"];
        $_SESSION['snow'][$myIndex]['marque'] = $aSnow[0]["brand"];

        $_SESSION['snow'][$myIndex]['modele'] = $aSnow[0]["model"];
        $_SESSION['snow'][$myIndex]['taille'] = $aSnow[0]["snowLength"];

        $_SESSION['snow'][$myIndex]['photo'] = $aSnow[0]["photo"];


        $marque = $_SESSION['snow'][$myIndex]['marque'];
        $modele = $_SESSION['snow'][$myIndex]['modele'];
        $taille = $_SESSION['snow'][$myIndex]['taille'];
        $selectQty = $_SESSION['snow'][$myIndex]['marque'];
        $photo = $_SESSION['snow'][$myIndex]['photo'];
        $myIndex = 0;

        $_SESSION['success'] = array(
            'marque' => $marque,
            'modele' => $modele,
            'taille' => $taille,
            'qtySel' => $selectQty,
            'success' => 'Success'
        );
/*
        $_SESSION['success']['marque'] = $marque;
        $_SESSION['success']['modele'] = $modele;
        $_SESSION['success']['taille'] = $taille;
        $_SESSION['success']['qtySel'] = $selectQty;
        $_SESSION['success']['success'] = 'Success';
*/
        // require 'view/snow.php';


        getSnows();
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }


}


function delPanier()
{
    // session_destroy();

    $_GET['action'] = "home";

    unset($_SESSION['snow']);
    unset($_SESSION["success"]);
    require 'view/home.php';
}

function command()
{
/*    $id = $_POST["id"];
    $qty = $_POST["qtySelect"];*/

    require_once "model/modele_snows.php";
    try {
        $aSnow = updateSnow();
        require 'view/snow.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}


function gestion()
{
   // $yes =$adminSnowRequest;
    require_once "model/modele_snows.php";
    try {
        $allSnows = extractSnows();
        require 'view/adminSnows.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }
}


function adminASnow()
{
    $code=$_GET["code"];
    require_once "model/modele_snows.php";
    try {
        $aSnow = adminExtractASnow($code);

        require 'view/adminASnow.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }
}

function adminUpdateSnow($code,$snow,$image)
{
    //$image = $_FILES['uploadfile']['name'];
    //$temp_name = $_FILES['uploadfile']['tmp_name'];

    if ($image["image"]["name"] != "") {

        require_once "model/modele_snows.php";
        $imageName=photo($image);

    }

    require_once "model/modele_snows.php";
    try {
        adminUpdateASnow($code,$snow,$image);
        require 'view/adminSnows.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }
}
