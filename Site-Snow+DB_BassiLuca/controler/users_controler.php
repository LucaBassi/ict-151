<?php


session_start();


function home()
{
    require "view/home.php";
}




function goLogin()
{
    require "view/login.php";
}

function logout()
{
    session_destroy();
    unset($_SESSION['user']);
    home();
}

function goToUploadImage()
{
    require "ImageStorer/index_image.php";
}

function adminPage()
{
    require "adminPage.php";
}



function userPage()
{
    require "view/userPage.php";
}

/*
function checkLogin()
{
    require "modeles/modele_users.php";
    login();
 //   require "userPage.php";
}
*/


function login($login)
{
    $_GET['loginError'] = false;
   // $youpi = $_POST['loginEmail'];

    if (isset($login['loginEmail']) && isset($login['loginPassword'])) {
        $userEmailAddress = $login['loginEmail'];
        $userPsw = $login['loginPassword'];
        require_once "model/modele_users.php";
        if (checkLogin($userEmailAddress, $userPsw)) {
            createSession($userEmailAddress);
            home();

        } else {
            $_GET['loginError'] = true;
            $_GET['action'] = "login";
            require "view/login.php";
        }

    } else {
        $_GET['action'] = "login";
        require "view/login.php";
    }
}


function register($register)
{

    if (isset($register['registerUsername'])) {
        if (isset($register['registerEmail']) && isset($register['registerPassword']) && isset($register['registerPassword2'])) {

            $registerUsername = $register['registerUsername'];
            $registerEmail = $register['registerEmail'];
            $registerPassword = $register['registerPassword'];
            $registerPassword2 = $register['registerPassword2'];

            if ($registerPassword == $registerPassword2) {
                require_once "model/modele_users.php";
                if (registerNewAccount($registerEmail, $registerPassword , $registerUsername)) {

                    createSession($registerEmail);
                    $_GET['registerError'] = false;
                    home();
                }
            } else {
                $_GET['registerError'] = true;
                $_GET['action'] = "register";
                require "view/register.php";
            }
        } else {
            $_GET['action'] = "register";
            require "view/register.php";
        }
    } else {
        $_GET['action'] = "register";
        require "view/register.php";
    }
}


function createSession($email)
{
    $_SESSION['user']['userEmailAddress'] = $email;

     $userType = getUserType($email);
     $_SESSION['user']['user_type'] = $userType;

    isLoggedIn();
}


function isAdmin()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'admin') {
        return true;
    } else {
        return false;
    }
}

function isUser()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'user') {


        return true;

    } else {
        return false;
    }
}


function isLoggedIn()
{
    if (isset($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}
