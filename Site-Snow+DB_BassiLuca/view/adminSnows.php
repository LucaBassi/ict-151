<?php
/*
Autor   : Luca.BASSI
Date    : 28.02.2020
*/
ob_start();
?>

    <link rel="stylesheet" href="view/content/scripts/bootstrap/css/bootstrap.css"></>
<?php


if (isset($_POST['error'])) {
    $error = $_POST['error'];
    echo "<h4 style='color: firebrick'>$error</h4>";
}
?>



    <table class="greyGridTable">
        <thead>
        <tr style="border-bottom: #0e0e0e ;border=4px">
            <th style="border-bottom: #0e0e0e;border=4px">Marque</th>
            <th>Modèle</th>
            <th>Taille</th>
            <th>Code</th>
            <th>Quantité</th>
            <th>.-/jour</th>
            <th>Description</th>
            <th>Photo</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <!--        <tfoot>
                <tr>
                    <td>foot1</td>
                    <td>foot2</td>
                    <td>foot3</td>
                    <td>foot4</td>
                </tr>
                </tfoot>-->
        <tbody>
        <?php
        if (isset($allSnows)) {
            foreach ($allSnows as $snows): ?>
                <tr>
                    <td><?= $snows["brand"] ?></td>
                    <td><?= $snows["model"] ?></td>
                    <td><?= $snows["code"] ?></td>
                    <td><?= $snows["snowLength"] ?></td>
                    <td><?= $snows["qtyAvailable"] ?></td>
                    <td><?= $snows["dailyPrice"] ?></td>
                    <td style="max-width: 100px"><?= $snows["description"] ?></td>
                    <td><img style="max-width: 100px" src="<?= $snows["photo"] ?>" alt=""></td>
                    <td><a href=index.php?action=adminASnow&code=<?= $snows['code'] ?> class="general foundicon-edit"></a>
                    </td>
                    <td><a href="index.php?action=" class="general foundicon-remove"></a></td>
                </tr>
            <?php endforeach;
        }
        ?>
        </tbody>
    </table>

    <script>

        function showDiv() {
            document.getElementById('welcomeDiv').style.display = "block";
        }

        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }</script>
<?php
$contenu = ob_get_clean();
require_once "gabarit.php";
?>