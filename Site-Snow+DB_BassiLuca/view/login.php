<?php

ob_start();
?>

    <h2>Login</h2>
<?php if (@$_GET['loginError'] == true) :?>
    <h5><span style="color:red">Login refusé</span></h5>
<?php endif ?>
    <article>
        <form class='form' method='POST' action="../index.php?action=login">
            <div class="container">
                <label for="userEmail"><b>Username</b><br>
                <input type="email" placeholder="Enter email address" name="loginEmail" required>
                </label>
                <label for="userPsw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="loginPassword" required>
            </div>
            <div class="container">
                <button type="submit" class="btn btn-default">Login</button>
                <button type="reset" class="btn btn-default">Reset</button>
                <span class="psw">Forgot <a href="#">password?</a></span>
            </div>
        </form>
        <div class="container signin">
            <p>Besoin d'un compte <a href="../index.php?action=register">Register</a>.</p>
        </div>
    </article>












    <div class="header">
        <h3 style="color: red">
            <?php
            if (isset($_SESSION['msg'])) {
                echo $_SESSION['msg'];
            }; ?>
        </h3>
    </div>




<?php
$contenu = ob_get_clean();
require "gabarit.php";
