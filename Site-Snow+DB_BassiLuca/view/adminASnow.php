<?php
/*
Autor   : Luca.BASSI
Date    : 28.02.2020
*/
ob_start();
?>


    <div class="row-fluid">
        <div class="span12">
            <div id="contentInnerSeparator"></div>
        </div>
    </div>

    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <h3>Modification d'un Snow</h3>
            </div>
            <form method="post" action="index.php?action=adminUpdateSnow&id=<?= $aSnow[0]['id'] ?>" enctype="multipart/form-data">
                <section id="contact">

                    <input type="file" id="files" name="image"/>
                    <img id="image" />

                   <script>

                    document.getElementById("files").onchange = function () {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("image").src = e.target.result;
                    };

                    // read the image file as a data URL.

                        reader.readAsDataURL(this.files[0]);

                    };
                   </script>
                </section>
                <?php
                if (isset($aSnow)) {
                foreach ($aSnow

                as $snow){
                ?>
                <img src="<?= $snow['photo'] ?>" alt="">
                <div class="row-fluid">
                    <form action="/action_page.php">
                        <input type="file" id="image" name="filename">
                        <input type="submit">
                    </form>
                    <!--Edit Sidebar Content here-->
                    <div class="span3">




                        <label>Marque</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['brand'] ?>" name="brand">
                        <br>
                        <label>Modele</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['model'] ?>" name="model">
                        <br>
                        <label>Code</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['code'] ?>" name="code">
                        <br>
                        <label>This is a label.</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['snowLength'] ?>" name="snowLength">
                        <br>
                        <label>This is a label.</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['qtyAvailable'] ?>" name="qtyAvailable">
                        <br>
                        <label>This is a label.</label>
                        <input type="text" placeholder="Standard Input" value="<?= $snow['dailyPrice'] ?>" name="dailyPrice">

                    </div>
                    <!--/End Sidebar Content -->

                    <!--Edit Main Content Area here-->
                    <div class="span9" id="divMain">



                        <label for="message">Description</label><textarea rows="11" name="description" id="message"
                                                                          class="input-block-level"
                                                                          placeholder="Comments"><?= $snow['description'] ?></textarea>

                        <label class="checkbox">
                            <?php
                            if ($snow['active'] == 1) {
                                ?>
                                <input type="hidden" name="active" value="1">
                                <?php
                                $check = 'checked';
                            } else {
                                $check = 'unchecked';
                                ?>
                                <input type="hidden" name="active" value="0">
                                <?php
                            }
                            ?>
                            <input type="checkbox" checked="<?= $check ?>" value="" name="active"> Check me out
                        </label>

                        <?php
                        }
                        }
                        ?>
                    </div>
                </div>
                <input type="submit" placeholder="modifier">
            </form>
        </div>
    </div>

    <div id="footerInnerSeparator"></div>

<?php
$contenu = ob_get_clean();
require_once "gabarit.php";
?>