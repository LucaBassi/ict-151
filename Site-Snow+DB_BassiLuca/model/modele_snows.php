<?php
/*
Autor   : Luca.BASSI
Date    : 28.02.2020
*/


function extractSnows()
{
    $snowsQuery = 'SELECT * FROM snows.snows';

    require_once 'model/connector.php';
    $snows = executeQuerySelect($snowsQuery);

    return $snows;
}


function extractASnow($id)
{

    $snowsQuery = "SELECT * FROM snows.snows where snows.id='$id'";

    require_once 'model/connector.php';
    $snows = executeQuerySelect($snowsQuery);

    return $snows;
}

function updateSnow()
{
    $snows = null;
    $params = null;
    $index = 0;

    foreach ($_SESSION['snow'] as $snow) {
        $newQty = $_SESSION['snow'][$index]['totalQty'] - $_SESSION["snow"][$index]["qty"];
        $params[$index] = array
        (
            ':id' => $_SESSION["snow"][$index]["id"],
            ':qty' => $newQty
        );
        $index++;
    }

    foreach ($params as $param) {

        $commandQuery = "UPDATE snows.snows SET qtyAvailable = :qty WHERE id = :id";

        require_once 'model/connector.php';
        $snows = executeQueryUpdate($commandQuery, $param);
    }
    unset($_SESSION['snow']);
    return $snows;
}


function adminExtractASnow($code)
{

    $snowsQuery = "SELECT * FROM snows.snows where snows.code='$code'";

    require_once 'model/connector.php';
    $snows = executeQuerySelect($snowsQuery);


    return $snows;

}

function adminUpdateASnow($code, $snow, $image)
{
    $imagePath = null;

    if ($image["image"]["name"] == !"") {
        $imageName=$_FILES["image"]["name"];
        $imagePath = 'view/content/images/' .$imageName;
    }

    $params = null;


    $params = array
    (
        ':code' => $snow["code"],
        ':brand' => $snow["brand"],
        ':model' => $snow["model"],
        ':snowLength' => $snow["snowLength"],
        ':qtyAvailable' => $snow["qtyAvailable"],
        ':description' => $snow["description"],
        ':dailyPrice' => $snow["dailyPrice"],
        ':photo' => $imagePath,
        ':active' => $snow["active"]
    );


    $updateQuery = "UPDATE snows.snows SET code = :code, brand = :brand, model = :model, snowLength = :snowLength , qtyAvailable = :qtyAvailable, description = :description, dailyPrice = :dailyPrice, model = :model, photo = :photo, active = :active WHERE code = :code ";

    require_once 'model/connector.php';
    executeQueryUpdate($updateQuery, $params);


}


function photo($image)
{
    //configure le dossier de reception des images

    $dir = "view/content/images/";
    $image = $_FILES['image']['name'];
    $temp_name = $_FILES['image']['tmp_name'];
    $imageName=$_FILES['image']['tmp_name'];

    if ($image != "") {
        if (file_exists($dir . $image)) {
            $image = time() . '_' . $image;
            $_FILES["image"]["name"]=$image;
        }

        $fdir = $dir . $image;
        move_uploaded_file($temp_name, $fdir);


    }
    return $imageName;
}