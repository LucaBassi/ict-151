<?php


require 'controler/users_controler.php';

if (isset($_GET['action'])) {
    switch ($_GET['action']) {

        case 'adminPage' :
            adminPage();
            break;

        case 'userPage' :
            userPage();
            break;

        case 'login' :
            login($_POST);
            break;

        case 'register' :
            register($_POST);
            break;

        case 'logout' :
            logout();
            break;

        case 'getSnows':
            require_once 'controler/snows_controler.php';
            getSnows();
            break;

        case 'snow':
            require_once 'controler/snows_controler.php';
            snow();
            break;

        case 'addPanier' :
            require_once 'controler/snows_controler.php';
            addPanier();
            break;

        case 'goPanier' :
            require_once 'controler/snows_controler.php';
            goPanier();
            break;

        case 'delPanier' :
            require_once 'controler/snows_controler.php';
            delPanier();
            break;

        case 'command' :
            require_once 'controler/snows_controler.php';
            command();
            break;


        case 'gestion' :
            require_once 'controler/snows_controler.php';
            gestion();
            break;

        case 'adminUpdateSnow' :
            require_once 'controler/snows_controler.php';
            adminUpdateSnow($_GET,$_POST,$_FILES);
            break;

        case 'adminASnow' :
            require_once 'controler/snows_controler.php';
            adminASnow();
            break;


        default :
            home();
            break;

    }
} else {
    home();
}
