<?php


session_start();


function home()
{
    require "view/home.php";
}

function details()
{
    require "details.php";
}

function meteo()
{
    require "meteo.php";
}

function transports()
{
    require "transports.php";
}

function nouveau()
{
    require "nouveau.php";
}

function listeDePresence()
{
    require "liste_de_presence.php";
}

function goLogin()
{
    require "view/login.php";
}

function logout()
{
    session_destroy();
    unset($_SESSION['user']);
    home();
}

function goToUploadImage()
{
    require "ImageStorer/index_image.php";
}

function adminPage()
{
    require "adminPage.php";
}

function listeImage()
{
    require "ImageStorer/liste.php";
}

/*
function posts()
{
    require "modeles/modele_posts.php";
    $posts= getPosts();
    require "post.php";
}
*/


function userPage()
{
    require "view/userPage.php";
}

/*
function checkLogin()
{
    require "modeles/modele_users.php";
    login();
 //   require "userPage.php";
}
*/


function login($loginRequest)
{

    if (isset($loginRequest['inputUserEmailAddress']) && isset($loginRequest['inputUserPsw'])) {
        //extract login parameters
        $userEmailAddress = $loginRequest['inputUserEmailAddress'];
        $userPsw = $loginRequest['inputUserPsw'];

        //try to check if user/psw are matching with the database
        require_once "model/modele_users.php";
        if (checkLogin($userEmailAddress, $userPsw)) {
            createSession($userEmailAddress);
            $_GET['loginError'] = false;
            $_GET['action'] = "home";
            require "view/home.php";
        } else { //if the user/psw does not match, login form appears again
            $_GET['loginError'] = true;
            $_GET['action'] = "login";
            require "view/login.php";
        }

    } else { //the user does not yet fill the form
        $_GET['action'] = "login";
        require "view/login.php";
    }
}

/**
 * This fonction is designed
 * @param $registerRequest
 */
function register($registerRequest)
{
    //variable set
    if (isset($registerRequest['inputUsername'])) {
        if (isset($registerRequest['inputUserEmailAddress']) && isset($registerRequest['inputUserPsw']) && isset($registerRequest['inputUserPswRepeat'])) {

            //extract register parameters
            $username = $registerRequest['inputUsername'];
            $userEmailAddress = $registerRequest['inputUserEmailAddress'];
            $userPsw = $registerRequest['inputUserPsw'];
            $userPswRepeat = $registerRequest['inputUserPswRepeat'];

            if ($userPsw == $userPswRepeat) {
                require_once "modeles/modele_users.php";
                if (registerNewAccount($username, $userEmailAddress, $userPsw)) {
                    createSession($userEmailAddress);
                    $_GET['registerError'] = false;
                    $_GET['action'] = "home";
                    require "view/home.php";
                }
            } else {
                $_GET['registerError'] = true;
                $_GET['action'] = "register";
                require "view/register.php";
            }
        } else {
            $_GET['action'] = "register";
            require "view/register.php";
        }
    }
    else {
        $_GET['action'] = "register";
        require "view/register.php";}
}

/**
 * This function is designed to create a new user session
 * @param $userEmailAddress : user unique id
 */
function createSession($userEmailAddress)
{
    $_SESSION['user']['userEmailAddress'] = $userEmailAddress;
    //set user type in Session
   /* $userType = getUserType($userEmailAddress);
    $_SESSION['user']['user_type'] = $userType;
   */
   isLoggedIn();
}


function isAdmin()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'admin') {
        return true;
    } else {
        return false;
    }
}

function isUser()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'user') {


        return true;

    } else {
        return false;
    }
}


function isLoggedIn()
{
    if (isset($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}
