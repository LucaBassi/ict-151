<?php
/*
Autor   : Luca.BASSI
Date    : 18.06.2020
*/

function getClients()
{
    $clientsQuery = 'SELECT * FROM agencies.customers';

    require_once 'model/connector.php';
    $clients = executeQuerySelect($clientsQuery);

    return $clients;
}

function removeAClient($id)
{
    $clientHaveAnAccount = false;

    if (checkIfCustomerHaveAnAccount($id) == true) {
        $clientHaveAnAccount = true;
        return $clientHaveAnAccount;
    } else {
        $removeClientsQuery = 'DELETE FROM `agencies`.`customers` WHERE  `id`= ' . $id . ';';
        require_once 'model/connector.php';
        $clients = executeQuerySelect($removeClientsQuery);
        return $clients;
    }
}


function checkIfCustomerHaveAnAccount($id)
{
    $checkIfAccountExistById = 'Select * FROM `agencies`.`agencies` WHERE  `id`= ' . $id . ';';
    require_once 'model/connector.php';
    $check = executeQuerySelect($checkIfAccountExistById);
    if ($check == null) {
        $checkIfAccountExist = false;
    } else {
        $checkIfAccountExist = true;
    }
    return $check;
}