<?php
/*
Autor   : Luca.BASSI
Date    : 19.06.2020
*/
function getAgencies()
{
    $agenciesQuery = 'SELECT * FROM agencies.customers';

    require_once 'model/connector.php';
    $agencies = executeQuerySelect($agenciesQuery);

    return $agencies;
}