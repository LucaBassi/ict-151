<?php
/*
Autor   : Luca.BASSI
Date    : 18.06.2020
*/

function clients()
{
    try {
        require_once "model/clients_model.php";
        $clients = getClients();
        $clientPage = "clientsContent.php";
        require 'view/clients_pages/clients.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }
}


function removeClient()
{
    $id = $_GET["id"];

    try {
        require_once "model/clients_model.php";
        removeAClient($id);
        clients();

    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }
}
