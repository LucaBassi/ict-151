<?php


function goLogin()
{
    require "view/login.php";
}

function logout()
{
    session_destroy();
    unset($_SESSION['user']);
    home();
}

function goToUploadImage()
{
    require "ImageStorer/index_image.php";
}

function adminPage()
{
    require "adminPage.php";
}


function userPage()
{
    require "view/userPage.php";
}

/*
function checkLogin()
{
    require "modeles/model_users.php";
    login();
 //   require "userPage.php";
}
*/


function login($login)
{
    $_GET['loginError'] = false;
    // $youpi = $_POST['loginEmail'];

    if (isset($login['login']) && isset($login['password'])) {
        $userLogin = $login['login'];
        $userPassword = $login['password'];
        require_once "model/model_users.php";


        include_once 'model/class.crud.php';
       // require_once 'model/connector.php';
       // $dbConnexion = openDBConnexion();//op
        $crud = new crud($dbConnexion);
        $crud->dataview();
        if (checkLogin($userLogin, $userPassword) == true) {
            createSession($userLogin);
            require "controller/navigation.php";
            home();

        } else {
            $_GET['loginError'] = true;
            $_GET['action'] = "login";
            require "view/login.php";
        }

    } else {
        $_GET['action'] = "login";
        require "view/login.php";
    }
}


function register($register)
{

    if (isset($register['registerUsername'])) {
        if (isset($register['registerEmail']) && isset($register['registerPassword']) && isset($register['registerPassword2'])) {

            $registerUsername = $register['registerUsername'];
            $registerEmail = $register['registerEmail'];
            $registerPassword = $register['registerPassword'];
            $registerPassword2 = $register['registerPassword2'];

            if ($registerPassword == $registerPassword2) {
                require_once "model/model_users.php";
                if (registerNewAccount($registerEmail, $registerPassword, $registerUsername)) {

                    createSession($registerEmail);
                    $_GET['registerError'] = false;
                    home();
                }
            } else {
                $_GET['registerError'] = true;
                $_GET['action'] = "register";
                require "view/register.php";
            }
        } else {
            $_GET['action'] = "register";
            require "view/register.php";
        }
    } else {
        $_GET['action'] = "register";
        require "view/register.php";
    }
}


function createSession($userLogin)
{
    $_SESSION['user'] = $userLogin;

    //$userType = getUserType($userLogin);
    //$_SESSION['user']['user_type'] = $userType;

    isLoggedIn();
}


function isAdmin()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'admin') {
        return true;
    } else {
        return false;
    }
}

function isUser()
{
    if (isset($_SESSION['user']) && $_SESSION['user']['user_type'] == 'user') {


        return true;

    } else {
        return false;
    }
}


function isLoggedIn()
{
    if (isset($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}
