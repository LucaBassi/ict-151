<?php
session_start();
if (isset($_GET['data'])){
    $result = $_GET['data'];
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {

        case 'login' :
            require "controller/users_controler.php";
            login($_POST);
            break;

        case 'clients' :
            require "controller/clients_controller.php";
            clients();
            break;

        case 'removeClient' :
            require "controller/clients_controller.php";
            removeClient();
            break;

        default :
            require "controller/navigation.php";
            home();
            break;

    }
} else {
    require "controller/navigation.php";
    home();
}

