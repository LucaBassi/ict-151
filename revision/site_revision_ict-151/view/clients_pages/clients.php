<?php
ob_start();
?>

    <section id="intro" class="main">
        <div class="spotlight">
            <div class="content">
                <header class="major">
                    <h2>Section Clients</h2>
                    <?php if (isset($clientPage)){
                        include $clientPage ;
                    } ?>
                </header>
            </div>
        </div>
    </section>

<?php
$content = ob_get_clean();
require "./view/gabarit.php";


?>