<?php
/*
Autor   : Luca.BASSI
Date    : 12.06.2020
*/

ob_start();

?>

<!-- Form -->
<div style="margin: 2%">
<section>
    <h2>Form</h2>
    <form method="post" action="index.php?action=login">
        <div class="row gtr-uniform">
            <div class="col-6 col-12-xsmall">
                <input type="text" name="login" id="login" value="" placeholder="Username" />
            </div>
            <br>
            <div class="col-6 col-12-xsmall">
                <input type="text" name="password" id="password" value="" placeholder="Password" />
            </div>
            <div class="col-12">
                <ul class="actions">
                    <li><input type="submit" value="Send Message" class="primary" /></li>
                    <li><input type="reset" value="Reset" /></li>
                </ul>
                <br><br>
            </div>
        </div>
    </form>
</section>

</div>
<?php
$content = ob_get_clean();
require "gabarit.php";
?>
