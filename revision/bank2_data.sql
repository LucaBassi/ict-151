-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.20 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour agencies
CREATE DATABASE IF NOT EXISTS `agencies` /*!40100 DEFAULT CHARACTER SET latin1 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `agencies`;

-- Listage de la structure de la table agencies. accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `id_customer` int NOT NULL,
  `id_agency` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comtes_clients1_idx` (`id_customer`),
  KEY `fk_comtes_agences1_idx` (`id_agency`),
  CONSTRAINT `fk_comtes_agences1` FOREIGN KEY (`id_agency`) REFERENCES `agencies` (`id`),
  CONSTRAINT `fk_comtes_clients1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table agencies.accounts : ~10 rows (environ)
DELETE FROM `accounts`;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`id`, `amount`, `id_customer`, `id_agency`) VALUES
	(1, 2013, 2, 5),
	(2, -25, 24, 2),
	(3, 14346, 17, 1),
	(4, 652, 4, 1),
	(5, 75, 24, 8),
	(6, 345, 24, 3),
	(7, 4024, 32, 6),
	(8, 9167, 10, 6),
	(9, 591, 11, 8),
	(10, -193, 29, 4);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;

-- Listage de la structure de la table agencies. agencies
CREATE TABLE IF NOT EXISTS `agencies` (
  `id` int NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table agencies.agencies : ~8 rows (environ)
DELETE FROM `agencies`;
/*!40000 ALTER TABLE `agencies` DISABLE KEYS */;
INSERT INTO `agencies` (`id`, `name`, `city`, `active`) VALUES
	(1, 'CS Remparts', 'Yverdon', 1),
	(2, 'CS Gare', 'Lausanne', 1),
	(3, 'CS Haldiman', 'Bulle', 1),
	(4, 'CS Malley', 'Lausanne', 1),
	(5, 'UBS Bel Air', 'Yverdon', 1),
	(6, 'UBS Bel Air', 'Genève', 1),
	(7, 'UBS Gare', 'Genève', 1),
	(8, 'UBS Gare', 'Lausanne', 1);
/*!40000 ALTER TABLE `agencies` ENABLE KEYS */;

-- Listage de la structure de la table agencies. customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Listage des données de la table agencies.customers : ~29 rows (environ)
DELETE FROM `customers`;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `name`, `surname`, `city`) VALUES
	(1, 'Dupond', 'Jean', 'Lausanne-Gare'),
	(2, 'Ansermet', 'Diane', 'Yverdon'),
	(3, 'Bresson', 'Kevin', 'Payerne'),
	(4, 'Diardon', 'Paul', 'Yverdon'),
	(5, 'Mellico', 'Sarah', 'Lausanne-Ouchy'),
	(6, 'Ernistor', 'Léa', 'Orbe'),
	(7, 'Durant', 'Henri', 'Yverdon'),
	(8, 'Duschmoll', 'Lara', 'Orbe'),
	(9, 'Duraton', 'Alex', 'Cossonay'),
	(10, 'Ernidiscal', 'Fred', 'Lausanne-Gare'),
	(11, 'Modetaite', 'Kelly', 'Lausanne-Gare'),
	(12, 'Filedeferre', 'Phil', 'Yverdon'),
	(13, 'Granduc', 'Estelle', 'Payerne'),
	(14, 'Histairy', 'Rod', 'Lausanne-Ouchy'),
	(15, 'Ilparre', 'Carlo', 'Cossonay'),
	(16, 'Karensac', 'Julie', 'Yverdon'),
	(17, 'Lunedemielle', 'Céline', 'Yverdon'),
	(18, 'Martin', 'Philippe', 'Sainte-Croix'),
	(19, 'Jaccard', 'René', 'Sainte-Croix'),
	(20, 'Nesépaz', 'Olivier', 'Cossonay'),
	(21, 'Otravo', 'Virginie', 'Fiez'),
	(22, 'Paletan', 'Eva', 'Genève'),
	(23, 'Quissé', 'Quentin', 'Pully'),
	(24, 'Radetaire', 'Léo', 'Mex'),
	(25, 'Sordulo', 'Lucas', 'Yverdon'),
	(29, 'Tapalasolusse', 'Sandra', 'Lausanne-Gare'),
	(30, 'Uzinahgas', 'Paola', 'Orbe'),
	(31, 'Kafecho', 'Ivan', 'Orbe'),
	(32, 'Vasipa', 'Cunégonde', 'Genève');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
